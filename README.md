# SecretDetection

This project seeks to illustrate the secret detection capabilities of GitLab

Examples are extracted from [gitlab secret detection QA](https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/tree/master/qa/fixtures/secrets)

## References

GitLab Secret Detection[[1]]  

[1]:https://gitlab.com/gitlab-org/security-products/analyzers/secrets